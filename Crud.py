from Conexion_db import Conexion_db


class Crud:
    def __init__(self,host,db,user,passwd):
        self.my_conn = Conexion_db(host,db,user,passwd)
        #creando un objeto de la clase conexion_db y definiendolo
        #como atributo de la clase Crud (self)
    
    #escribir metodos para escribir, leer, eliminar,actualizar registros de tablas
    #segun las necesidades de mi proyecto

    def insertar_pasajero(self, nombre,direccion,telefono,fecha_nacimiento):
        query = "INSERT INTO \"Pasajeros\""+\
            "(nombre,direccion,telefono,fecha_nacimiento)"+\
                "VALUES ('"+nombre+ "','"+direccion+\
                    "','"+telefono+"','"+fecha_nacimiento+"')"
        print(query)#for test purposes
        self.my_conn.escribir_db(query)

    def leer_buses(self): #los metodos llevan como parametro a self
        query = "SELECT id,modelo, capacidad, placa, marca"+\
            " FROM \"Buses\" ORDER BY id ASC;"
        respuesta=self.my_conn.consultar_db(query)
        return respuesta

    def leer_viajes(self):
        query = "SELECT id, id_pasajero, id_trayecto, inicio, fin"+\
            " FROM \"Viajes\";"
        respuesta=self.my_conn.consultar_db(query) #lista de tuplas         
        return respuesta

    def cerrar(self):
        self.my_conn.cerrar_db()